package com.project_ex01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectEx01Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectEx01Application.class, args);
	}

}
